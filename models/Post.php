<?php
    class Post {
        //db stuff
        private $conn;
        private $table = 'myblog.posts';

        //Post properties
        public $id;
        public $category_id;
        //public $category_name;
        public $title;
        public $body;
        public $author;
        public $created_at;

        //constructor with db
        public function __construct($db) {
            $this->conn = $db;
        }

        //get posts
        public function read() {
            //create query
            $query = 'SELECT 
                p.id,
                p.category_id,
                p.title,
                p.body,
                p.author,
                p.created_at
            FROM 
                myblog.posts p
            ORDER BY
                p.created_at DESC';
            
            //$query = 'SELECT * FROM myblog.posts';
                
            
            
            // prepare statement
            $stmt = $this->conn->prepare($query);

            //execute query
            $stmt->execute();
            
            return $stmt;
        }

        //get single post by id
        public function read_single() {
            //create query
            $query = 'SELECT 
                p.id,
                p.category_id,
                p.title,
                p.body,
                p.author,
                p.created_at
            FROM 
                myblog.posts p
            WHERE
                p.id = ?
            LIMIT 0,1';

            //prepare statement
            $stmt = $this->conn->prepare($query);

            //bind id
            $stmt->bindParam(1,$this->id);

            //execute query
            $stmt->execute();

            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            //set properties
            $this->title = $row['title'];
            $this->body = $row['body'];
            $this->author = $row['author'];
            $this->category_id = $row['category_id'];
        }

        //get single post by author
        public function read_author() {
            //create query
            $query = 'SELECT 
                p.id,
                p.category_id,
                p.title,
                p.body,
                p.author,
                p.created_at
            FROM 
                myblog.posts p
            WHERE
                p.author = ?
            LIMIT 0,1';

            //prepare statement
            $stmt = $this->conn->prepare($query);

            //bind id
            $stmt->bindParam(1,$this->author);

            //execute query
            $stmt->execute();

            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            //set properties
            $this->id = $row['id'];
            $this->title = $row['title'];
            $this->body = $row['body'];
            $this->category_id = $row['category_id'];
        }


        //create post
        public function create() {
            //query
            $query = 'INSERT INTO myblog.posts SET 
                title = :title,
                body = :body,
                author = :author,
                category_id = :category_id';

            //prepare statement
            $stmt = $this->conn->prepare($query);

            //clean data
            $this->title = htmlspecialchars(strip_tags($this->title));
            $this->body = htmlspecialchars(strip_tags($this->body));
            $this->author = htmlspecialchars(strip_tags($this->author));
            $this->category_id = htmlspecialchars(strip_tags($this->category_id));

            //bind data
            $stmt->bindParam(':title', $this->title);
            $stmt->bindParam(':body', $this->body);
            $stmt->bindParam(':author', $this->author);
            $stmt->bindParam(':category_id', $this->category_id);

            //execute
            if($stmt->execute()) {
                return true;
            }

            //print error
            printf("Error: %s.\n", $stmt->error);

            return false;
        }

        //update post
        public function update() {
            //query
            $query = 'UPDATE myblog.posts SET 
                title = :title,
                body = :body,
                author = :author,
                category_id = :category_id
            WHERE 
                id = :id';
            

            //prepare statement
            $stmt = $this->conn->prepare($query);

            //clean data
            $this->title = htmlspecialchars(strip_tags($this->title));
            $this->body = htmlspecialchars(strip_tags($this->body));
            $this->author = htmlspecialchars(strip_tags($this->author));
            $this->category_id = htmlspecialchars(strip_tags($this->category_id));
            $this->id = htmlspecialchars(strip_tags($this->id));

            //bind data
            $stmt->bindParam(':title', $this->title);
            $stmt->bindParam(':body', $this->body);
            $stmt->bindParam(':author', $this->author);
            $stmt->bindParam(':category_id', $this->category_id);
            $stmt->bindParam(':id', $this->id);

            //execute
            if($stmt->execute()) {
                return true;
            }

            //print error
            printf("Error: %s.\n", $stmt->error);

            return false;
        }

        //delete
        public function delete() {
            //query
            $query = 'DELETE FROM myblog.posts WHERE id = :id';

            //prepare statement
            $stmt = $this->conn->prepare($query);

            //clean data
            $this->id = htmlspecialchars(strip_tags($this->id));
            
            //bind data
            $stmt->bindParam(':id', $this->id);

            //execute
            if($stmt->execute()) {
                return true;
            }

            //print error
            printf("Error: %s.\n", $stmt->error);

            return false;
        }

    }