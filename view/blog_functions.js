'use strict';

var path = 'http://127.0.0.1/php_rest_myblog/';

function show_all() {
    var textToDisplay = '';

    $.getJSON('http://127.0.0.1/php_rest_myblog/api/post/read.php', function(result) {
        $.each(result, function(i, field) {
            for (var j=0; j<field.length; j++) {
                textToDisplay = textToDisplay + 'id: ' + field[j].id + '<br/> title: ' + field[j].title + ' <br/> author: ' + field[j].author + '<br/> text: ' + field[j].body + '<br/> category id: ' + field[j].category_id + '<br/><br/>';
                
            }
            document.getElementById('resultfield').innerHTML= textToDisplay;
        });
    });
    
}

function search_post() {
    var textId = document.getElementById("search-id").value;
    var path = 'http://127.0.0.1/php_rest_myblog/api/post/read_single.php?id='+textId+'';
    var textToDisplay = '';
    $.getJSON(path, function(result) {
        $.each(result, function(i, field) {
            console.log(i);
            console.log(field);
            textToDisplay = textToDisplay + ' ' + i + ': ' + field + '<br>';
                
            //$("div").append(field + " ");
            document.getElementById('resultfield').innerHTML= textToDisplay;
        });
    });
}

function search_post_by_author() {
    console.log('got the right author search function');
    var author = document.getElementById("search-author").value;
    var path = 'http://127.0.0.1/php_rest_myblog/api/post/read_author.php?author='+author+'';
    var textToDisplay = '';
    $.getJSON(path, function(result) {
        $.each(result, function(i, field) {
            console.log(i);
            console.log(field);
            textToDisplay = textToDisplay + ' ' + i + ': ' + field + '<br>';
                
            //$("div").append(field + " ");
            document.getElementById('resultfield').innerHTML= textToDisplay;
        });
    });
}

function add_post() {
    var createTitle = document.getElementById("create-title").value;
    var createAuthor = document.getElementById("create-author").value;
    var createText = document.getElementById("create-text").value;
    var createCatId = document.getElementById("create-cat-id").value;


    var data = {"title": createTitle, "author": createAuthor, "body": createText, "category_id": createCatId};

    var settings = {
        "url": "http://127.0.0.1/php_rest_myblog/api/post/create.php",
        "method": "POST",
        "timeout": 0,
        "headers": {
        "Content-Type": "application/json"
        },
        "data": JSON.stringify(data),
    };
    
    $.ajax(settings).done(function (response) {
        console.log(response);
    });
    document.getElementById('resultfield').innerHTML = 'Data was successfully added to database.';

    
}

function update_post() {
    var updateId = document.getElementById("update-id").value;
    var updateTitle = document.getElementById("update-title").value;
    var updateAuthor = document.getElementById("update-author").value;
    var updateText = document.getElementById("update-text").value;
    var updateCatId = document.getElementById("update-cat-id").value;

    
    var data = {"title": updateTitle, "body": updateText, "author": updateAuthor, "category_id": updateCatId, "id": updateId};

    var settings = {
        "url": "http://127.0.0.1/php_rest_myblog/api/post/update.php",
        "method": "PUT",
        "timeout": 0,
        "headers": {
          "Content-Type": "application/json"
        },
        "data": JSON.stringify(data),
      };
      
      $.ajax(settings).done(function (response) {
        console.log(response);
      });

      document.getElementById('resultfield').innerHTML = 'Data was successfully updated to database.';
}

function delete_post() {
    var deleteId = document.getElementById("delete-id").value;
    console.log(deleteId);
    var data = {"id": deleteId};
    var settings = {
        "url": "http://127.0.0.1/php_rest_myblog/api/post/delete.php",
        "method": "DELETE",
        "timeout": 0,
        "headers": {
          "Content-Type": "application/json"
        },
        "data": JSON.stringify(data),
      };
      
      $.ajax(settings).done(function (response) {
        console.log(response);
        document.getElementById('resultfield').innerHTML = 'Data was successfully deleted from database.';
      });

      
}