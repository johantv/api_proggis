The database used in this project is called myblog and it has two tables: posts and categories (categories not in use yet, maybe in the future).

Api requests are as follows:
1. To read all posts that exist in database
    GET http://127.0.0.1/php_rest_myblog/api/post/read.php
2. To get a post by author (in this case author is antsu)
    GET http://127.0.0.1/php_rest_myblog/api/post/read_author.php?author=antsu
3. To get a post by id (in this case id is 3)
    GET http://127.0.0.1/php_rest_myblog/api/post/read_single.php?id=3
4. To add a new post in database:
    POST http://127.0.0.1/php_rest_myblog/api/post/create.php
    with json including title, body, author and category_id: 
    {
    	"title": "My tech post",
    	"body": "This is body",
    	"author": "Johanna",
    	"category_id": "1"
    }
5. To update an existing post in database:
    PUT http://127.0.0.1/php_rest_myblog/api/post/update.php
    with json including title, body, author, category_id and post id:
    {
    	"title": "Updated post",
    	"body": "This is an updated body",
    	"author": "Johanna",
    	"category_id": "1",
    	"id": "4"
    }
6. To delete a post from database:
    DELETE http://127.0.0.1/php_rest_myblog/api/post/delete.php
    with json including post id:
    {
	    "id": "7"
    }
    
Database queries can be found in Post.php file.
