<?php
    class Database {
        //db params
        private $host = '127.0.0.1';
        private $db_name = 'myblog';
        private $username = 'root';
        private $password = '';
        private $conn;

        //https://www.youtube.com/watch?v=OEWXbpUMODk
        // db connect
        public function connect() {
            $this->conn = null;
            
            try {
                $this->conn = new PDO('mysql:host=' . $this->host . ';dbname = ' . $this->db_name, $this->username, $this->password);
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } 
            catch(PDOException $e) {
                echo 'Connection error: ' . $e->getMessage();
            }
            //tsekkaa kohdasta 12:12 eteenpäin syntaksivirheet

            return $this->conn;
        }
    }