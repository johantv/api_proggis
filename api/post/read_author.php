<?php
    //headers
    //allow by anybody
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    
    include_once '../../config/Database.php';
    include_once '../../models/Post.php';

    //instantiate db & connect
    $database = new Database();
    $db = $database->connect(); // connect function in db class
    
    //instantiate blog post object
    $post = new Post($db);

    //get author
    $post->author = isset($_GET['author']) ? $_GET['author'] : die();

    //get post
    $post->read_author();

    //create array
    $post_arr = array(
        'id' => $post->id,
        'title' => $post->title,
        'body' => $post->body,
        'author' => $post->author,
        'category_id' => $post->category_id
    );

    //make json
    print_r(json_encode($post_arr));
